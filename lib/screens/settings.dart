import 'package:flutter/material.dart';
import 'package:nasmejse/models/constants.dart';
import 'package:nasmejse/models/language.dart';
import 'package:nasmejse/models/language_list.dart';
import 'package:nasmejse/models/style.dart';

class Settings extends StatefulWidget {
  @override
  _SettingsState createState() => _SettingsState();
}

class _SettingsState extends State<Settings> {

  var darkThemeEnabled     = false;
  int currentLanguageIndex = 0;

  // ---------------------------------------------------------------------------

  void checkCurrentTheme() async{
    String currentTheme = await Style.getTheme();
    setState(() {
      darkThemeEnabled = currentTheme == 'dark' ? true : false;
    });
  }

  // ---------------------------------------------------------------------------

  void setTheme(String themeName){
    Style.setTheme(themeName);
    Style.setThemePrefs(themeName);
  }

  // ---------------------------------------------------------------------------

  void setLanguage(String language){
    Language.setLanguage(language);
    Language.saveLanguage(language);
  }

  // ---------------------------------------------------------------------------

  void checkCurrentLanguage() async{
    String currentLanguage = await Language.getLanguage();
    for(var item in Constants.languageList ) {
      if(item.value == currentLanguage){
        setState(() {
          currentLanguageIndex = item.index;
        });
        break;
      }
    }
  }

  // ---------------------------------------------------------------------------

  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    checkCurrentTheme();
    checkCurrentLanguage();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Style.scaffoldBackgroundColor,
      appBar: AppBar(
        title: Text(Language.settings),
        backgroundColor: Style.appBarBackground,
        elevation: 0.1,
      ),
      body: ListView(
        padding: EdgeInsets.all(20),
        children: <Widget>[
          Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: <Widget>[
              ListTile(
                title: Text(Language.darkTheme, style: TextStyle(color: Style.settingsTitle),),
                trailing: Switch(
                  value: darkThemeEnabled,
                  onChanged: (changeTheme) {
                    String setThemeNew = '';
                    if(changeTheme) {
                      setThemeNew = 'dark';
                      setState(() {
                        darkThemeEnabled = true;
                      });
                    }
                    else {
                      setThemeNew = 'light';
                      setState(() {
                        darkThemeEnabled = false;
                      });
                    }
                    setTheme(setThemeNew);
                  },
                ),
              ),
              Divider(color: Style.dividerColor),
              ListTile(
                title: Text(Language.language, style: TextStyle(color: Style.settingsTitle),),
              ),
              Container(
                height: (Constants.languageList.length * 60).toDouble(),
                child: ListView.builder(
                  physics: const NeverScrollableScrollPhysics(),
                  itemCount: Constants.languageList.length,
                  itemBuilder: (BuildContext context, int index) {
                    LanguageList languageListItem = Constants.languageList[index];
                    return RadioListTile(
                      title: Text(languageListItem.name, style: TextStyle(color: Style.settingsTitle)),
                      activeColor: Style.formRadioActiveColor,
                      groupValue: currentLanguageIndex,
                      value: languageListItem.index,
                      onChanged: (val) {
                        setState(() {
                          currentLanguageIndex = val;
                          setLanguage(languageListItem.value);
                        });
                      },
                    );
                  }
                ),
              ),
              Divider(color: Style.dividerColor),
            ],
          )
        ],
      ),
    );
  }
}