import 'dart:async';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:nasmejse/models/constants.dart';
import 'package:nasmejse/models/joke_category.dart';
import 'package:nasmejse/models/joke.dart';
import 'package:nasmejse/models/joke_sort.dart';
import 'package:nasmejse/models/loader.dart';
import 'package:nasmejse/models/style.dart';
import 'package:nasmejse/screens/send.dart';
import 'package:nasmejse/screens/settings.dart';
import 'package:nasmejse/screens/view.dart';
import 'package:nasmejse/screens/category.dart';

class HomeFutureBuilder extends StatefulWidget {
  @override
  _HomeFutureBuilderState createState() => _HomeFutureBuilderState();
}

class _HomeFutureBuilderState extends State<HomeFutureBuilder> {

  Future<List<Joke>> jokes;
  Future<List<JokeCategory>> jokeCategories;

  // ---------------------------------------------------------------------------

  getJokes(){
    jokes = Joke.getList(0, '');
    return jokes;
  }

  // ---------------------------------------------------------------------------

  Future<void> refreshJokes() async {
    setState(() {
      jokes = getJokes();
    });
  }

  // ---------------------------------------------------------------------------

  void setSortJokes(String sortValue){
    Joke.setSort(sortValue);
    refreshJokes();
  }

  // ---------------------------------------------------------------------------

  void checkCurrentTheme() async{
    String currentTheme = await Style.getTheme();
    setState(() {
      Style.setTheme(currentTheme);
    });
  }

  // ---------------------------------------------------------------------------

  @override
  void initState() {
    super.initState();
    jokes          = getJokes();
    jokeCategories = JokeCategory.getList();
  }

  // ---------------------------------------------------------------------------

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Style.scaffoldBackgroundColor,
      appBar: AppBar(
        title: Image.asset(
          'assets/logo.png',
          height: 45.0, alignment: Alignment.center,
        ),
        centerTitle: true,
        iconTheme: IconThemeData(color: Style.appBarIconColor),
        backgroundColor: Style.appBarBackgroundHome, //Colors.white,
        elevation: 0.1,
        bottom: PreferredSize(child: Container(color: Style.appBarBorderBottomColor, height: 1.0,), preferredSize: Size.fromHeight(4.0)),
        actions: <Widget>[
          /*IconButton (
            icon: Icon(Icons.refresh),
            color: ThemeColors.appBarIconColor,
            onPressed: () {
              setState(() {
                refreshJokes();
              });
            },
          ),*/
          Container(
            padding: EdgeInsets.fromLTRB(0, 0, 20, 0),
            child: PopupMenuButton(
                color: Colors.lightBlue,
                child: Icon(Icons.tune),
                onSelected: setSortJokes,
                itemBuilder: (BuildContext context){
                  return Constants.sortListJoke.map((JokeSort jokeSort){
                    return PopupMenuItem(
                      value: jokeSort.value,
                      child: Text(jokeSort.name),
                      textStyle: TextStyle(color: Colors.white),
                    );
                  }).toList();
                }
            ),
          ),
        ],
      ),
      drawer: Drawer(
        child: SafeArea(
          child: Column(
            children: [
              Expanded(
                flex: 4,
                child: Container(
                  color: Style.drawerBackgroundColor,
                  padding: EdgeInsets.fromLTRB(20, 0, 20, 0),
                  child: Center(
                    child: Column(
                      mainAxisAlignment: MainAxisAlignment.center,
                      crossAxisAlignment: CrossAxisAlignment.center,
                      children: <Widget>[
                        Image.asset(
                          'assets/logo.png',
                          width: 250.0, alignment: Alignment.center,
                        ),
                        SizedBox(height: 15.0),
                        Text(
                          'Svaki osmeh produžava život pet minuta. Sa nama živite večno!',
                          style: TextStyle(color: Colors.grey[400], height: 1.4),
                        ),
                      ],
                    ),
                  ),
                ),
              ),
              FutureBuilder(
                future: jokeCategories,
                // ignore: missing_return
                builder: (BuildContext context, AsyncSnapshot snapshot){
                  switch(snapshot.connectionState){
                    case ConnectionState.none:
                    case ConnectionState.waiting:
                    case ConnectionState.active:
                      return Center(child: Loader.show());
                    case ConnectionState.done:
                      if(snapshot.hasError) return Expanded(
                        flex: 12,
                        child: Container(
                          color: Colors.white,
                        ),
                      );
                      var jokeCategories = snapshot.data;
                      return Expanded(
                        flex: 12,
                        child: Container(
                          color: Style.drawerBackgroundColor,
                          padding: EdgeInsets.all(0.0),
                          child: ListView.builder(
                              itemCount: jokeCategories.length,
                              itemBuilder: (BuildContext context, int index) {
                                JokeCategory jokeCategory = jokeCategories[index];
                                return Padding(
                                  padding: EdgeInsets.fromLTRB(0, 0, 0, 0),
                                  child: Container(
                                    decoration: BoxDecoration(
                                      border: Border(top: BorderSide(color: Style.drawerNavigationBorderColor)),
                                      color: Style.drawerBackgroundColor,
                                    ),
                                    child: ListTile(
                                      title: Text(jokeCategory.name, style: TextStyle(color: Style.drawerNavigationColor)),
                                      leading: Icon(Icons.label, color: Colors.lightBlue,),
                                      trailing: Chip(
                                        label: Text(jokeCategory.numberOfJokes, style: TextStyle(color: Colors.black26, fontWeight: FontWeight.bold),),
                                        backgroundColor: Colors.grey[200],
                                      ),
                                      onTap: () {
                                        Navigator.of(context).pop();
                                        Navigator.push(context, MaterialPageRoute(
                                            builder: (BuildContext context) => Category(jokeCategory.id, jokeCategory.name)
                                        ));
                                        //Navigator.push(context, MaterialPageRoute(builder: ( context) => MessageCompose()));
                                      },
                                    ),
                                    /*child: InkWell(
                                      splashColor: Colors.lightBlue[900],
                                      onTap: () {
                                        print('tu sam');
                                      },
                                      child: Container(
                                        height: 50.0,
                                        child: Row(
                                          mainAxisAlignment: MainAxisAlignment.spaceBetween,
                                          children: <Widget>[
                                            Row(
                                              children: <Widget>[
                                                //SizedBox(width: 10.0),
                                                Icon(Icons.arrow_forward_ios),
                                                Padding(
                                                  padding: const EdgeInsets.all(8.0),
                                                  child: Text(
                                                    category.name,
                                                    style: TextStyle(
                                                      fontSize: 16.0,
                                                    ),
                                                  ),
                                                ),
                                              ],
                                            ),
                                            Icon(Icons.arrow_right),
                                          ],
                                        ),
                                      ),
                                    ),*/
                                  ),
                                );
                              }
                          ),
                        ),
                      );
                  }
                },
              ),
            ],
          ),
        ),
      ),
      body: RefreshIndicator(
        child: FutureBuilder(
          future: jokes,
          // ignore: missing_return
          builder: (BuildContext context, AsyncSnapshot snapshot){
            switch(snapshot.connectionState){
              case ConnectionState.none:
              case ConnectionState.waiting:
              case ConnectionState.active:
                return Center(child: Loader.show());
              case ConnectionState.done:
                if(snapshot.hasError || snapshot.data.length == 0) return Center(
                  child: Column(
                    mainAxisAlignment: MainAxisAlignment.center,
                    crossAxisAlignment: CrossAxisAlignment.center,
                    children: <Widget>[
                      Text('Došlo je do greške prilikom učitavanja sadržaja.', style: TextStyle(color: Colors.grey),),
                      SizedBox(height: 15.0),
                      RaisedButton(
                        textColor: Colors.white,
                        color: Colors.lightBlue,
                        child: Text("Učitaj ponovo"),
                        onPressed: () {
                          setState(() {
                            refreshJokes();
                            jokeCategories = JokeCategory.getList();
                          });
                        },
                        shape: new RoundedRectangleBorder(
                          borderRadius: new BorderRadius.circular(30.0),
                        ),
                      ),
                    ],
                  ),
                );
                var jokes = snapshot.data;
                return ListView.separated (
                    itemCount: jokes.length,
                    separatorBuilder: (context, index) => Divider(color: Colors.grey[300]),
                    itemBuilder: (BuildContext context, int index) {
                      Joke joke = jokes[index];
                      return Padding(
                        padding: index == 0 ?  const EdgeInsets.fromLTRB(0, 10.0, 0, 0) :  const EdgeInsets.fromLTRB(0, 0, 0, 0),
                        child: ListTile(
                          isThreeLine: true,
                          //trailing: Text('T'),
                          leading: CircleAvatar(
                            backgroundColor: Colors.lightBlue,
                            child: Text(joke.categoryName.substring(0,2).toUpperCase(), style: TextStyle(color: Colors.white)),
                          ),
                          title: Padding(
                            padding: const EdgeInsets.fromLTRB(0, 0, 0, 5.0),
                            child: Text(
                              joke.name,
                              style: TextStyle(fontWeight: FontWeight.w500, color: Style.jokeName),
                            ),
                          ),
                          subtitle: Text(
                            joke.text,
                            maxLines: 3,
                            overflow: TextOverflow.ellipsis,
                            style: TextStyle(color: Style.jokeText, fontSize: 14, height: 1.3),
                          ),
                          onTap: () {
                            Navigator.push(context, MaterialPageRoute(builder: (BuildContext context) => View(joke.id, joke.name, joke.text, joke.categoryName, joke.date)));
                          },
                        ),
                      );
                    }
                );
            }
          },
        ),
        onRefresh: refreshJokes,
      ),
      bottomNavigationBar: BottomNavigationBar(
        onTap: (int index) async{
          switch(index){
            case 0:
              refreshJokes();
              break;
            case 1:
              Navigator.push(context, MaterialPageRoute(
                  builder: (BuildContext context) => Send()
              ));
              break;
            case 2:
              await Navigator.push(context, MaterialPageRoute(
                  builder: (BuildContext context) => Settings()
              ));
              checkCurrentTheme();
              break;
          }
        },
        backgroundColor: Style.bottomNavigationBarBackgroundColor,
        items: <BottomNavigationBarItem>[
          BottomNavigationBarItem(
            title: Text('Osveži', style: TextStyle(color: Style.bottomNavigationBarColor),),
            icon: Icon(Icons.refresh, color: Style.bottomNavigationBarColor,),
          ),
          BottomNavigationBarItem(
            title: Text('Pošalji vic', style: TextStyle(color: Style.bottomNavigationBarColor),),
            icon: Icon(Icons.send, color: Style.bottomNavigationBarColor,),
          ),
          BottomNavigationBarItem(
            title: Text('Podešavanja', style: TextStyle(color: Style.bottomNavigationBarColor),),
            icon: Icon(Icons.settings, color: Style.bottomNavigationBarColor,),
          ),
        ],
      ),
    );
  }
}
