import 'package:flutter/material.dart';
import 'package:nasmejse/models/language.dart';
import 'package:nasmejse/models/loader.dart';
import 'package:nasmejse/models/style.dart';
import 'package:nasmejse/models/user.dart';


class Loading extends StatefulWidget {
  @override
  _LoadingState createState() => _LoadingState();
}

class _LoadingState extends State<Loading> {

  // number of seconds for loader
  int loaderTime = 1;

  // ---------------------------------------------------------------------------

  void showLoader() async{

    // -------------------------------------------------------------------------

    // set theme
    String theme = await Style.getTheme();
    Style.setTheme(theme);

    // -------------------------------------------------------------------------

    // set language
    String language = await Language.getLanguage();
    Language.setLanguage(language);

    // -------------------------------------------------------------------------

    // check user
    String user = await User.checkUser();
    if(user == ''){
      user = await User.setUser();
    }
    User.recordUserVisits(user);

    // -------------------------------------------------------------------------

    // loader
    Future.delayed(Duration(seconds: loaderTime), () {
      Navigator.pushReplacementNamed(context, '/home');
    });

  }

  // ---------------------------------------------------------------------------

  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    showLoader();
  }

  // ---------------------------------------------------------------------------

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colors.white,
      body: Center(
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          children: <Widget>[
            Image.asset('assets/logo.png', width: 300.0),
            SizedBox(height: 30.0),
            Loader.show(),
          ],
        ),
      ),
    );
  }

  // ---------------------------------------------------------------------------
}