import 'dart:async';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:loading_overlay/loading_overlay.dart';
import 'package:nasmejse/models/constants.dart';
import 'package:nasmejse/models/joke.dart';
import 'package:nasmejse/models/joke_sort.dart';
import 'package:nasmejse/models/language.dart';
import 'package:nasmejse/models/loader.dart';
import 'package:nasmejse/models/style.dart';
import 'package:nasmejse/models/joke_category.dart';
import 'package:nasmejse/screens/view.dart';

class Category extends StatefulWidget {

  final String categoryName;
  final String categoryId;

  Category(this.categoryId,this.categoryName);

  @override
  _CategoryState createState() => _CategoryState();
}

class _CategoryState extends State<Category> {

  int pageNumber                     = 1;
  List<Joke> jokes                   = [];
  List<JokeCategory> jokeCategories  = [];
  bool _loading                      = false;
  bool _loadingMore                  = false;
  ScrollController _scrollController = ScrollController();

  // ---------------------------------------------------------------------------

  void getJokes() async{
    if(pageNumber==1) {setState(() {_loading = true;});}
    List<Joke> _jokes = await Joke.getList(pageNumber, widget.categoryId);
    if(pageNumber==1) {setState(() {_loading = false;});}
    setState(() {
      if(_jokes != null){
        jokes.addAll(_jokes);
        pageNumber = pageNumber + 1;
        if(_jokes.isEmpty) _loadingMore = false;
      } else {
        jokes          = _jokes;
        jokeCategories = [];
      }
    });
  }

  // ---------------------------------------------------------------------------

  Future<void> refreshJokes() async {
    setState((){
      jokes        = [];
      pageNumber   = 1;
      _loadingMore = false;
    });

    if (_scrollController.hasClients) _scrollController.jumpTo(1);
    getJokes();
  }

  // ---------------------------------------------------------------------------

  void setSortJokes(String sortValue){
    Joke.setSort(sortValue);
    refreshJokes();
  }

  // ---------------------------------------------------------------------------

  @override
  void initState() {
    super.initState();

    getJokes();

    _scrollController.addListener(() {
      if(_scrollController.position.pixels == _scrollController.position.maxScrollExtent && _scrollController.position.pixels>0){
        setState(() {_loadingMore = true;});
        getJokes();
      }
    });
  }

  // ---------------------------------------------------------------------------

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Style.scaffoldBackgroundColor,
      appBar: AppBar(
        title: Text(widget.categoryName),
        backgroundColor: Style.appBarBackground,
        elevation: 0.1,
        actions: <Widget>[
          IconButton (
            icon: Icon(Icons.refresh),
            color: Colors.white,
            onPressed: () {
              setState(() {
                refreshJokes();
              });
            },
          ),
          Container(
            padding: EdgeInsets.fromLTRB(0, 0, 20, 0),
            child: PopupMenuButton(
              color: Colors.lightBlue,
              child: Icon(Icons.tune),
              onSelected: setSortJokes,
              itemBuilder: (BuildContext context){
                return Constants.sortListJoke.map((JokeSort jokeSort){
                  return PopupMenuItem(
                    value: jokeSort.value,
                    child: Text(jokeSort.name),
                    textStyle: TextStyle(color: Colors.white),
                  );
                }).toList();
              }
            ),
              /*PopupMenuButton<String>(
              child: Icon(Icons.tune),
              onSelected: setSortJokes,
              itemBuilder: (BuildContext context){
                return Constants.sortList.map((String sort){
                  return PopupMenuItem<String>(
                    value: sort,
                    child: Text(sort),
                  );
                }).toList();
              },
            ),*/
          ),
        ],
      ),
      body: LoadingOverlay(
        child: RefreshIndicator(
          child: jokes == null
              ? Center(
            child: Column(
              mainAxisAlignment: MainAxisAlignment.center,
              crossAxisAlignment: CrossAxisAlignment.center,
              children: <Widget>[
                Text(Language.contentLoadingError, style: TextStyle(color: Colors.grey),),
                SizedBox(height: 15.0),
                RaisedButton(
                  textColor: Colors.white,
                  color: Colors.lightBlue,
                  child: Text(Language.contentReload),
                  onPressed: () {
                    setState(() {
                      refreshJokes();
                    });
                  },
                  shape: new RoundedRectangleBorder(
                    borderRadius: new BorderRadius.circular(30.0),
                  ),
                ),
              ],
            ),
          )
              :  ListView.separated(
            controller: _scrollController,
            separatorBuilder: (context, index) => Divider(color: Colors.grey[300]),
            //itemExtent: 50,
            itemBuilder: (context, index){
              if(index == jokes.length){
                return Loader.show();
              }
              Joke joke = jokes[index];
              return Padding(
                padding: index == 0 ?  const EdgeInsets.fromLTRB(0, 10.0, 0, 0) :  const EdgeInsets.fromLTRB(0, 0, 0, 0),
                child: ListTile(
                  isThreeLine: true,
                  //trailing: Text('T'),
                  leading: CircleAvatar(
                    backgroundColor: Colors.lightBlue,
                    child: Text(joke.categoryName.substring(0,2).toUpperCase(), style: TextStyle(color: Colors.white)),
                  ),
                  title: Padding(
                    padding: const EdgeInsets.fromLTRB(0, 0, 0, 5.0),
                    child: Text(
                      joke.name,
                      style: TextStyle(fontWeight: FontWeight.w500, color: Style.jokeName),
                    ),
                  ),
                  subtitle: Text(
                    joke.text,
                    maxLines: 3,
                    overflow: TextOverflow.ellipsis,
                    style: TextStyle(color: Style.jokeText, fontSize: 14, height: 1.3),
                  ),
                  onTap: () {
                    Navigator.push(context, MaterialPageRoute(builder: (BuildContext context) => View(joke.id, joke.name, joke.text, joke.categoryName, joke.date)));
                  },
                ),
              );
            },
            itemCount: _loadingMore ? jokes.length+1 : jokes.length, //jokes.length+1,
          ),
          onRefresh: refreshJokes,
        ),
        isLoading: _loading,
        opacity: 1,
        color: Style.scaffoldBackgroundColor,
        progressIndicator: Loader.show(),
      ),
    );
  }

}
