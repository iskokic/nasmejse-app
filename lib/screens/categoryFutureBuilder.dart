import 'dart:async';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:nasmejse/models/constants.dart';
import 'package:nasmejse/models/joke.dart';
import 'package:nasmejse/models/joke_sort.dart';
import 'package:nasmejse/models/loader.dart';
import 'package:nasmejse/models/style.dart';
import 'package:nasmejse/screens/view.dart';

class CategoryFutureBuilder extends StatefulWidget {

  final String categoryName;
  final String categoryId;

  CategoryFutureBuilder(this.categoryId,this.categoryName);

  @override
  _CategoryFutureBuilderState createState() => _CategoryFutureBuilderState();
}

class _CategoryFutureBuilderState extends State<CategoryFutureBuilder> {

  Future<List<Joke>> jokes;

  // ---------------------------------------------------------------------------

  getJokes() {
    jokes = Joke.getList(0, widget.categoryId);
    return jokes;
  }

  // ---------------------------------------------------------------------------

  Future<void> refreshJokes() async {
    setState(() {
      jokes = getJokes();
    });
  }

  // ---------------------------------------------------------------------------

  void setSortJokes(String sortValue){
    Joke.setSort(sortValue);
    refreshJokes();
  }

  // ---------------------------------------------------------------------------

  @override
  void initState() {
    super.initState();
    jokes = getJokes();
  }

  // ---------------------------------------------------------------------------

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Style.scaffoldBackgroundColor,
      appBar: AppBar(
        title: Text(widget.categoryName),
        backgroundColor: Style.appBarBackground,
        elevation: 0.1,
        actions: <Widget>[
          IconButton (
            icon: Icon(Icons.refresh),
            color: Colors.white,
            onPressed: () {
              setState(() {
                refreshJokes();
              });
            },
          ),
          Container(
            padding: EdgeInsets.fromLTRB(0, 0, 20, 0),
            child: PopupMenuButton(
                child: Icon(Icons.tune),
                onSelected: setSortJokes,
                itemBuilder: (BuildContext context){
                  return Constants.sortListJoke.map((JokeSort jokeSort){
                    return PopupMenuItem(
                      value: jokeSort.value,
                      child: Text(jokeSort.name),
                    );
                  }).toList();
                }
            ),
            /*PopupMenuButton<String>(
              child: Icon(Icons.tune),
              onSelected: setSortJokes,
              itemBuilder: (BuildContext context){
                return Constants.sortList.map((String sort){
                  return PopupMenuItem<String>(
                    value: sort,
                    child: Text(sort),
                  );
                }).toList();
              },
            ),*/
          ),
        ],
      ),
      body: RefreshIndicator(
        child: FutureBuilder(
          future: jokes,
          // ignore: missing_return
          builder: (BuildContext context, AsyncSnapshot snapshot){
            switch(snapshot.connectionState){
              case ConnectionState.none:
              case ConnectionState.waiting:
              case ConnectionState.active:
                return Center(child: Loader.show());
              case ConnectionState.done:
                if(snapshot.hasError || snapshot.data.length == 0) return Center(
                  child: Column(
                    mainAxisAlignment: MainAxisAlignment.center,
                    crossAxisAlignment: CrossAxisAlignment.center,
                    children: <Widget>[
                      Text('Došlo je do greške prilikom učitavanja sadržaja.', style: TextStyle(color: Colors.grey),),
                      SizedBox(height: 15.0),
                      RaisedButton(
                        textColor: Colors.white,
                        color: Colors.lightBlue,
                        child: Text("Učitaj ponovo"),
                        onPressed: () {
                          setState(() {
                            refreshJokes();
                          });
                        },
                        shape: new RoundedRectangleBorder(
                          borderRadius: new BorderRadius.circular(30.0),
                        ),
                      ),
                    ],
                  ),
                );
                var jokes = snapshot.data;
                return ListView.separated (
                    itemCount: jokes.length,
                    separatorBuilder: (context, index) => Divider(color: Colors.grey[300]),
                    itemBuilder: (BuildContext context, int index) {
                      Joke joke = jokes[index];
                      return Padding(
                        padding: index == 0 ?  const EdgeInsets.fromLTRB(0, 10.0, 0, 0) :  const EdgeInsets.fromLTRB(0, 0, 0, 0),
                        child: ListTile(
                          //contentPadding: EdgeInsets.fromLTRB(10.0, 5.0, 10.0, 5.0),
                          isThreeLine: true,
                          //trailing: Text('T'),
                          leading: CircleAvatar(
                            backgroundColor: Colors.lightBlue,
                            child: Text('${index + 1}', style: TextStyle(color: Colors.white)),
                          ),
                          title: Padding(
                            padding: const EdgeInsets.fromLTRB(0, 0, 0, 5.0),
                            child: Text(
                              joke.name,
                              style: TextStyle(fontWeight: FontWeight.w500, color: Style.jokeName),
                            ),
                          ),
                          subtitle: Text(
                            joke.text,
                            maxLines: 3,
                            overflow: TextOverflow.ellipsis,
                            style: TextStyle(color: Style.jokeText, fontSize: 14, height: 1.3),
                          ),
                          onTap: () {
                            Navigator.push(context, MaterialPageRoute(builder: (BuildContext context) => View(joke.id, joke.name, joke.text, joke.categoryName, joke.date)));
                          },
                        ),
                      );
                    }
                );
            }
          },
        ),
        onRefresh: refreshJokes,
      ),
    );
  }

}
