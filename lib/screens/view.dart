import 'package:flutter/material.dart';
import 'package:loading_overlay/loading_overlay.dart';
import 'package:nasmejse/models/joke.dart';
import 'package:nasmejse/models/language.dart';
import 'package:nasmejse/models/loader.dart';
import 'package:nasmejse/models/style.dart';
import 'package:nasmejse/services/webservice.dart';
import 'package:share/share.dart';
import 'package:smooth_star_rating/smooth_star_rating.dart';
import 'package:fluttertoast/fluttertoast.dart';
import 'dart:convert';

class View extends StatefulWidget {

  final String id;
  final String name;
  final String text;
  final String categoryName;
  final String date;
  const View(this.id, this.name, this.text, this.categoryName, this.date);

  @override
  _ViewState createState() => _ViewState();
}

class _ViewState extends State<View> {

  bool _isLoading = false;
  double rating   = 0.0;

  // ---------------------------------------------------------------------------

  rateJoke(double ratingValue) async{

    setState(() {_isLoading = true;});

    String message = Language.ratingJokeError;

    Webservice jokeRating = await Joke.rating(widget.id, ratingValue);
    setState(() {_isLoading = false;});
    if(jokeRating.status){
      String messageWebservice = json.decode(jokeRating.data)['message'];
      print(messageWebservice);
      if(messageWebservice == 'success'){
        message = Language.ratingJokeSuccess;
        setState(() {
          rating = ratingValue;
        });
      } else {
        message = Language.ratingJokeAlreadyRated;
      }
    }

    Fluttertoast.showToast(
        msg: message,
        toastLength: Toast.LENGTH_SHORT,
        gravity: ToastGravity.BOTTOM,
        timeInSecForIos: 1,
        backgroundColor: Colors.lightBlue,
        textColor: Colors.white,
        fontSize: 16.0
    );
  }

  // ---------------------------------------------------------------------------

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Style.scaffoldBackgroundColor,
      appBar: AppBar(
        title: Text(Language.back),
        backgroundColor: Style.appBarBackground,
        elevation: 0.1,
        actions: <Widget>[
          IconButton(
            icon: Icon(Icons.share),
            color: Colors.white,
            onPressed: () {
              Share.share(widget.name + '\n\n' + widget.text + Language.appSignature, subject: widget.name);
            },
          ),
        ],
      ),
      body: LoadingOverlay(
        child: ListView(
          padding: EdgeInsets.all(20),
          children: <Widget>[
            Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: <Widget>[
                Text(
                  widget.name,
                  style: TextStyle(fontWeight: FontWeight.w500,
                    color: Style.jokeName,
                    fontSize: 18,),
                ),
                SizedBox(height: 10),
                Text(
                    Language.category + ': ${widget.categoryName}'.toUpperCase(),
                  style: TextStyle(fontWeight: FontWeight.w500,
                    color: Colors.grey,
                    fontSize: 11,),
                ),
                SizedBox(height: 3),
                Text(
                    Language.date + ': ${widget.date}'.toUpperCase(),
                  style: TextStyle(fontWeight: FontWeight.w500,
                    color: Colors.grey,
                    fontSize: 11,),
                ),
                SizedBox(height: 5),
                Divider(color: Style.dividerColor),
                SizedBox(height: 5),
                Text(
                  widget.text,
                  style: TextStyle(
                      color: Style.jokeText, fontSize: 16, height: 1.5),
                ),
                SizedBox(height: 30),
                SmoothStarRating(
                    allowHalfRating: false,
                    onRatingChanged: (value) {
                      rateJoke(value);
                    },
                    starCount: 5,
                    rating: rating,
                    size: 40.0,
                    filledIconData: Icons.star,
                    halfFilledIconData: Icons.blur_on,
                    color: Colors.lightBlue,
                    borderColor: Colors.lightBlue,
                    spacing: 0.0
                )
              ],
            )
          ],
        ),
        isLoading: _isLoading,
        // demo of some additional parameters
        opacity: 0.3,
        progressIndicator: Loader.show(),
      ),
    );
  }
}