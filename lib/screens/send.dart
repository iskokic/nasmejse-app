import 'package:flutter/material.dart';
import 'package:loading_overlay/loading_overlay.dart';
import 'package:nasmejse/models/joke.dart';
import 'package:nasmejse/models/language.dart';
import 'package:nasmejse/models/loader.dart';
import 'package:nasmejse/models/style.dart';

class Send extends StatefulWidget {
  @override
  _SendState createState() => _SendState();
}

class _SendState extends State<Send> {

  bool _loading = false;
  String name   = '';
  String email  = '';
  String text   = '';
  final key     = GlobalKey<FormState>();

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Style.scaffoldBackgroundColor,
      appBar: AppBar(
        title: Text(Language.sendJoke),
        backgroundColor: Style.appBarBackground,
        elevation: 0.1,
      ),
      body: LoadingOverlay(
        child: SingleChildScrollView(
          child: Form(
            key: key,
            child: Container(
              padding: EdgeInsets.all(20),
              child: Column(
                children: <Widget>[
                  Text(Language.sendJokeText, style: TextStyle(color: Style.jokeText, fontSize: 16),),
                  SizedBox(height: 15),
                  Container(
                    child: TextFormField(
                      validator: (value) {
                        int len = value.length;
                        return len<1 ? Language.sendJokeFormNameInvalid : null;
                      },
                      onSaved: (value) => name = value,
                      decoration: InputDecoration(
                        labelText: Language.sendJokeFormName,
                        labelStyle: TextStyle(fontSize: 16, color: Style.formTextColor),
                        errorStyle: TextStyle(color: Style.formTextErrorColor),
                        enabledBorder: UnderlineInputBorder(
                          borderSide: BorderSide(color: Style.formBorderColor),
                        ),
                        focusedBorder: UnderlineInputBorder(
                          borderSide: BorderSide(color: Style.formBorderFocusedColor),
                        ),
                        errorBorder: UnderlineInputBorder(
                          borderSide: BorderSide(color: Style.formBorderErrorColor),
                        ),
                        focusedErrorBorder: UnderlineInputBorder(
                          borderSide: BorderSide(color: Style.formBorderFocusedErrorColor),
                        ),
                      ),
                      style: TextStyle(color: Style.formTextColor),
                    ),
                  ),
                  SizedBox(height: 15),
                  Container(
                    child: TextFormField(
                      validator: (value) {
                        bool emailValid = RegExp(r"^[a-zA-Z0-9.a-zA-Z0-9.!#$%&'*+-/=?^_`{|}~]+@[a-zA-Z0-9]+\.[a-zA-Z]+").hasMatch(value);
                        return !emailValid ? Language.sendJokeFormEmailInvalid : null;
                      },
                      onSaved: (value) => email = value,
                      decoration: InputDecoration(
                        labelText: Language.sendJokeFormEmail,
                        labelStyle: TextStyle(fontSize: 16, color: Style.formTextColor),
                        errorStyle: TextStyle(color: Style.formTextErrorColor),
                        enabledBorder: UnderlineInputBorder(
                          borderSide: BorderSide(color: Style.formBorderColor),
                        ),
                        focusedBorder: UnderlineInputBorder(
                          borderSide: BorderSide(color: Style.formBorderFocusedColor),
                        ),
                        errorBorder: UnderlineInputBorder(
                          borderSide: BorderSide(color: Style.formBorderErrorColor),
                        ),
                        focusedErrorBorder: UnderlineInputBorder(
                          borderSide: BorderSide(color: Style.formBorderFocusedErrorColor),
                        ),
                      ),
                      style: TextStyle(color: Style.formTextColor),
                    ),
                  ),
                  SizedBox(height: 15),
                  Container(
                    child: TextFormField(
                      validator: (value) {
                        int len = value.length;
                        return len<1 ? Language.sendJokeFormTextInvalid : null;
                      },
                      onSaved: (value) => text = value,
                      textAlignVertical: TextAlignVertical.top,
                      decoration: InputDecoration(
                        labelText: Language.sendJokeFormText,
                        labelStyle: TextStyle(fontSize: 16,  color: Style.formTextColor),
                        errorStyle: TextStyle(color: Style.formTextErrorColor),
                        enabledBorder: UnderlineInputBorder(
                          borderSide: BorderSide(color: Style.formBorderColor),
                        ),
                        focusedBorder: UnderlineInputBorder(
                          borderSide: BorderSide(color: Style.formBorderFocusedColor),
                        ),
                        errorBorder: UnderlineInputBorder(
                          borderSide: BorderSide(color: Style.formBorderErrorColor),
                        ),
                        focusedErrorBorder: UnderlineInputBorder(
                          borderSide: BorderSide(color: Style.formBorderFocusedErrorColor),
                        ),
                      ),
                      style: TextStyle(color: Style.formTextColor),
                      maxLines: 9,
                    ),
                  ),
                  SizedBox(height: 30),
                  RaisedButton(
                    textColor: Colors.white,
                    color: Colors.lightBlue,
                    child: Text(Language.sendJokeFormSend),
                    onPressed: () async{
                      if(this.key.currentState.validate()){
                        this.key.currentState.save();
                        setState(() {
                          _loading = true;
                        });
                        bool send = await Joke.send(name, email, text);
                        setState(() {
                          _loading = false;
                        });

                        String _message = send ? Language.sendJokeFormSendSuccess : Language.sendJokeFormSendError;
                        await showDialog<void>(
                          context: context,
                          builder: (BuildContext context) {
                            return AlertDialog(
                              title: Text(Language.sendJoke),
                              content: Text(_message),
                              actions: <Widget>[
                                FlatButton(
                                  child: Text(Language.close),
                                  onPressed: () {
                                    //Navigator.of(context).pop();
                                    Navigator.pop(context);
                                  },
                                ),
                              ],
                            );
                          },
                        );
                        if(send) Navigator.pop(context);

                        /*if(send) {
                          await showDialog<void>(
                            context: context,
                            builder: (BuildContext context) {
                              return AlertDialog(
                                title: Text(Language.sendJoke),
                                content: Text(Language.sendJokeFormSendSuccess),
                                actions: <Widget>[
                                  FlatButton(
                                    child: Text(Language.close),
                                    onPressed: () {
                                      //Navigator.of(context).pop();
                                      Navigator.pop(context);
                                    },
                                  ),
                                ],
                              );
                            },
                          );
                          Navigator.pop(context);
                        } else {
                          await showDialog<void>(
                            context: context,
                            builder: (BuildContext context) {
                              return AlertDialog(
                                title: Text(Language.sendJoke),
                                content: Text(Language.sendJokeFormSendError),
                                actions: <Widget>[
                                  FlatButton(
                                    child: Text(Language.close),
                                    onPressed: () {
                                      Navigator.of(context).pop();
                                    },
                                  ),
                                ],
                              );
                            },
                          );
                        }*/
                        //this.key.currentState.save();
                        //print(name + email + text);
                        //Message message = Message(subject, body);
                        //Navigator.pop(context, message);
                      }
                    },
                    shape: new RoundedRectangleBorder(
                      borderRadius: new BorderRadius.circular(30.0),
                    ),
                  ),
                ],
              ),
            ),
          ),
        ),
        isLoading: _loading,
        // demo of some additional parameters
        opacity: 0.3,
        progressIndicator: Loader.show(),
      ),
    );
  }
}
