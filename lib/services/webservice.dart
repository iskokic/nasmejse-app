import 'dart:convert';
import 'package:http/http.dart' as http;
import 'package:connectivity/connectivity.dart';

class Webservice{

  final bool status;
  final String data;

  Webservice(this.status, this.data);

  // ---------------------------------------------------------------------------

  static getData(String url, var params) async{
    bool status = false;
    String data = '';

    var connectivityResult = await (Connectivity().checkConnectivity());
    if (connectivityResult == ConnectivityResult.mobile || connectivityResult == ConnectivityResult.wifi) {
      try {
        http.Response response = await http.post(url, body: params);
        String responseData = response.body;

        bool responseStatus = json.decode(responseData)['status'];
        if(responseStatus) {
          status = true;
          data   = responseData;
        }
      } catch (e) {
        print(e);
      }
    }

    Webservice _data = Webservice(status, data);
    return _data;
  }

  // ---------------------------------------------------------------------------
}