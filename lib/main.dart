import 'package:flutter/material.dart';
import 'package:nasmejse/screens/loading.dart';
import 'package:nasmejse/screens/home.dart';

void main() => runApp(MaterialApp(
  initialRoute: '/',
  routes: {
    '/': (context)     => Loading(),
    '/home': (context) => Home(),
  },
));




