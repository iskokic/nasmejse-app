import 'package:shared_preferences/shared_preferences.dart';
import 'package:nasmejse/models/constants.dart';

class Language{

  static String appName                  = "";
  static String appDescription           = "";
  static String appSignature             = "";
  static String contentLoadingError      = "";
  static String contentReload            = "";
  static String refresh                  = "";
  static String settings                 = "";
  static String sendJoke                 = "";
  static String sendJokeText             = "";
  static String sendJokeFormName         = "";
  static String sendJokeFormNameInvalid  = "";
  static String sendJokeFormEmail        = "";
  static String sendJokeFormEmailInvalid = "";
  static String sendJokeFormText         = "";
  static String sendJokeFormTextInvalid  = "";
  static String sendJokeFormSend         = "";
  static String sendJokeFormSendSuccess  = "";
  static String sendJokeFormSendError    = "";
  static String ratingJokeSuccess        = "";
  static String ratingJokeError          = "";
  static String ratingJokeAlreadyRated   = "";
  static String category                 = "";
  static String date                     = "";
  static String back                     = "";
  static String close                    = "";
  static String darkTheme                = "";
  static String language                 = "";
  static String sortByName               = "";
  static String sortByDate               = "";
  static String sortByRand               = "";

  // ---------------------------------------------------------------------------

  static setLanguage(String name){
    switch(name){

      case 'english':
        Language.appName                  = "Nasmej se";
        Language.appDescription           = "Each smile extends life by five minutes. Live with us forever!";
        Language.appSignature             = "\n\nApplication NASMEJ SE\nPlace of good fun";
        Language.contentLoadingError      = "There was an error loading the content.";
        Language.contentReload            = "Reload";
        Language.refresh                  = "Refresh";
        Language.settings                 = "Settings";
        Language.sendJoke                 = "Send joke";
        Language.sendJokeText             = "If you would like to send us a joke and become part of the application team please fill out the form below. If you enter your first and last name it will appear under the title of the joke.";
        Language.sendJokeFormName         = "Name and surname";
        Language.sendJokeFormNameInvalid  = "Please enter your name and surname";
        Language.sendJokeFormEmail        = "E-mail address";
        Language.sendJokeFormEmailInvalid = "Please enter a valid email address";
        Language.sendJokeFormText         = "The text jokes";
        Language.sendJokeFormTextInvalid  = "Please enter a joke text";
        Language.sendJokeFormSend         = "Send";
        Language.sendJokeFormSendSuccess  = "The form was successfully submitted. Thanks.";
        Language.sendJokeFormSendError    = "NOTE: An error occured while submitting the form. Please try again.";
        Language.ratingJokeSuccess        = "You have successfully rated the joke. Thanks.";
        Language.ratingJokeError          = "An error occurred while evaluating. Please try again.";
        Language.ratingJokeAlreadyRated   = "Joke has already been successfully rated. Thanks.";
        Language.category                 = "Category";
        Language.date                     = "Date";
        Language.back                     = "Back";
        Language.close                    = "Close";
        Language.darkTheme                = "Dark theme";
        Language.sortByName               = "Sort by name";
        Language.sortByDate               = "Sort by date";
        Language.sortByRand               = "Sort by random";
        Language.language                 = "Language";
        break;

      default:
        Language.appName                  = "Nasmej se";
        Language.appDescription           = "Svaki osmeh produžava život pet minuta. Sa nama živite večno!";
        Language.appSignature             = "\n\nAplikacija NASMEJ SE\nMesto dobre zabave";
        Language.contentLoadingError      = "Došlo je do greške prilikom učitavanja sadržaja.";
        Language.contentReload            = "Učitaj ponovo";
        Language.refresh                  = "Osveži";
        Language.settings                 = "Podešavanja";
        Language.sendJoke                 = "Pošalji vic";
        Language.sendJokeText             = "Ukoliko želite da nam pošaljete vic i postanete deo tima aplikacije molimo popunite formu ispod. Ukoliko unesete Vaše ime i prezime ono će se naći ispod naslova vica.";
        Language.sendJokeFormName         = "Ime i prezime";
        Language.sendJokeFormNameInvalid  = "Molimo unesite ime i prezime";
        Language.sendJokeFormEmail        = "Email adresa";
        Language.sendJokeFormEmailInvalid = "Molimo unesite validnu email adresu";
        Language.sendJokeFormText         = "Tekst vica";
        Language.sendJokeFormTextInvalid  = "Molimo unesite tekst vica";
        Language.sendJokeFormSend         = "Pošalji";
        Language.sendJokeFormSendSuccess  = "Formular je uspešno poslat. Hvala.";
        Language.sendJokeFormSendError    = "NAPOMENA: Došlo je do greške prilikom slanja formulara. Molimo pokušajte ponovo.";
        Language.ratingJokeSuccess        = "Uspešno ste ocenili vic. Hvala.";
        Language.ratingJokeError          = "Došlo je do greške prilikom ocenjivanja. Molimo pokušajte ponovo.";
        Language.ratingJokeAlreadyRated   = "Vic je već uspešno ocenjen. Hvala.";
        Language.category                 = "Kategorija";
        Language.date                     = "Datum";
        Language.back                     = "Nazad";
        Language.close                    = "Zatvori";
        Language.darkTheme                = "Tamna tema";
        Language.language                 = "Jezik";
        Language.sortByName               = "Sortiraj po nazivu";
        Language.sortByDate               = "Sortiraj po datumu";
        Language.sortByRand               = "Sortiraj po slučajnom izboru";
        break;
    }
  }

  // ---------------------------------------------------------------------------

  static Future<String> getLanguage() async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    String themeName = prefs.get('language');
    if(themeName == null) {
      themeName = Constants.defaultLanguage;
    }

    return themeName;
  }

  // ---------------------------------------------------------------------------

  static Future<void> saveLanguage(String language) async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    await prefs.setString('language', language);
  }

  // ---------------------------------------------------------------------------
}