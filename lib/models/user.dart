import 'package:nasmejse/services/webservice.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'constants.dart';
import 'dart:math';
import 'dart:convert';
import 'package:crypto/crypto.dart';

class User {

  // ---------------------------------------------------------------------------

  static Future<String> checkUser() async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    String user = prefs.get('user');
    if(user == null) {
      user = '';
    }

    return user;
  }

  // ---------------------------------------------------------------------------

  static Future<String> setUser() async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    /*
    //import 'package:device_id/device_id.dart';
    String user = await DeviceId.getID;
    */

    var now         = new DateTime.now();
    var rand        = new Random().nextInt(1000);
    var code        = utf8.encode('$now $rand');
    var userPrepare = sha1.convert(code);
    String user     = '$userPrepare';

    await prefs.setString('user', user);

    return user;
  }

  // ---------------------------------------------------------------------------

  static Future<bool> recordUserVisits(String user) async {

    bool status = false;

    await Future.delayed(Duration(seconds: 2), () {});
    var params = {'token' : 'nasmejse', 'user': user};
    Webservice webservice = await Webservice.getData(Constants.urlRecordUserVisits, params);
    status = webservice.status;

    return status;
  }

  // ---------------------------------------------------------------------------
}