import 'package:flutter/material.dart';
import 'package:flutter_spinkit/flutter_spinkit.dart';

class Loader{

  // ---------------------------------------------------------------------------

  static show(){
    return SpinKitRing(
      color: Colors.lightBlue,
      size: 60.0,
    );
  }

  // ---------------------------------------------------------------------------

  static showDefault(){
    return CircularProgressIndicator();
  }

  // ---------------------------------------------------------------------------
}