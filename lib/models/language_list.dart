class LanguageList{
  final int index;
  final String value;
  final String name;

  const LanguageList({this.index, this.value, this.name});
}