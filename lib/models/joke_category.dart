import 'dart:convert';
import 'package:nasmejse/models/constants.dart';
import 'package:nasmejse/services/webservice.dart';

class JokeCategory {

  final String id;
  final String name;
  final String numberOfJokes;
  final String icon;

  JokeCategory(this.id, this.name, this.numberOfJokes, this.icon);

  // ---------------------------------------------------------------------------

  JokeCategory.fromJson(Map<String, dynamic> json):
        id            = json['id'],
        name          = json['name'],
        numberOfJokes = json['number_of_jokes'],
        icon          = json['icon'];

  // ---------------------------------------------------------------------------

  static Future<List<JokeCategory>> getList() async {

    var params = {'token' : 'nasmejse'};
    Webservice webservice = await Webservice.getData(Constants.urlJokeCategory, params);
    /*http.Response response = await http.post(Constants.urlJokeCategory, body: {
      'token': 'nasmejse',
    });
    String content = response.body;
    //List collection = json.decode(data)['data'];
    */
    List collection = [];
    List<JokeCategory> _categories;
    if(webservice.status){
      collection  = json.decode(webservice.data)['data'];
      _categories = collection.map((json) => JokeCategory.fromJson(json)).toList();
    }
    //List<JokeCategory> _categories = collection.map((json) => JokeCategory.fromJson(json)).toList();

    return _categories;
  }

  // ---------------------------------------------------------------------------
}