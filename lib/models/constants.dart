import 'package:nasmejse/models/joke_sort.dart';
import 'package:nasmejse/models/language.dart';
import 'language_list.dart';

class Constants{

  // ---------------------------------------------------------------------------

  // default params
  static const String defaultLanguage = "serbian";
  static const String defaultTheme    = "light";

  // ---------------------------------------------------------------------------

  // url
  static const String urlJokeList         = "http://www.nasmejse.com/webservices/nasmejse/application/joke_list";
  static const String urlJokeCategory     = "http://www.nasmejse.com/webservices/nasmejse/application/joke_category";
  static const String urlJokeSend         = "http://www.nasmejse.com/webservices/nasmejse/application/joke_send";
  static const String urlJokeRating       = "http://www.nasmejse.com/webservices/nasmejse/application/joke_rating";
  static const String urlRecordUserVisits = "http://www.nasmejse.com/webservices/nasmejse/application/record_user_visits";

  // ---------------------------------------------------------------------------

  // sort
  static  List<JokeSort> sortListJoke =  <JokeSort>[
    JokeSort(value: 'date', name: Language.sortByDate ),
    JokeSort(value: 'name', name: Language.sortByName),
    //JokeSort(value: 'rand', name: Language.sortByRand)
  ];

  // ---------------------------------------------------------------------------

  // language choice
  static List<LanguageList> languageList =  <LanguageList>[
    LanguageList(index: 1, value: 'serbian', name: 'Srpski' ),
    LanguageList(index: 2, value: 'english', name: 'English'),
  ];

  // ---------------------------------------------------------------------------

}