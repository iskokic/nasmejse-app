import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:nasmejse/models/constants.dart';

class Style{

  // colors
  static Color appBarBackground                   = Color(0xFF00A9F4);
  static Color appBarBackgroundHome               = Color(0xFFFFFFFF);
  static Color appBarIconColor                    = Color(0xFF00A9F4);
  static Color appBarBorderBottomColor            = Color(0xFFEEEEEE);
  static Color drawerBackgroundColor              = Color(0xFFFFFFFF);
  static Color drawerNavigationColor              = Color(0xDD000000);
  static Color drawerNavigationBorderColor        = Color(0xFFEEEEEE);
  static Color scaffoldBackgroundColor            = Color(0xFFFFFFFF);
  static Color jokeName                           = Color(0xDD000000);
  static Color jokeText                           = Color(0xFF757575);
  static Color dividerColor                       = Color(0xFFEEEEEE);
  static Color settingsTitle                      = Color(0xDD000000);
  static Color bottomNavigationBarBackgroundColor = Color(0xFF00A9F4);
  static Color bottomNavigationBarColor           = Color(0xFFEEEEEE);

  static Color formTextColor               = Color(0xFFFFFFFF);
  static Color formTextErrorColor          = Color(0xFF00A9F4);
  static Color formBorderColor             = Color(0xFFFFFFFF);
  static Color formBorderFocusedColor      = Color(0xFFFFFFFF);
  static Color formBorderFocusedErrorColor = Color(0xFF00A9F4);
  static Color formBorderErrorColor        = Color(0xFF00A9F4);
  static Color formRadioActiveColor        = Color(0xFF00A9F4);

  // ---------------------------------------------------------------------------

  static setTheme(String name){
    switch(name){
      case 'dark':
        Style.appBarBackground                   = Color(0xFF2B2B2B);
        Style.appBarBackgroundHome               = Color(0xFF2B2B2B);
        Style.appBarIconColor                    = Color(0xFFFFFFFF);
        Style.appBarBorderBottomColor            = Color(0xFF2B2B2B);
        Style.drawerBackgroundColor              = Color(0xFF3C3F41);
        Style.drawerNavigationColor              = Color(0xFFFFFFFF);
        Style.drawerNavigationBorderColor        = Color(0xFF777777);
        Style.scaffoldBackgroundColor            = Color(0xFF3C3F41);
        Style.jokeName                           = Color(0xFFFFFFFF);
        Style.jokeText                           = Color(0xFFBDBDBD);
        Style.dividerColor                       = Color(0xFF696B6D);
        Style.settingsTitle                      = Color(0xFFFFFFFF);
        Style.bottomNavigationBarBackgroundColor = Color(0xFF111111);
        Style.bottomNavigationBarColor           = Color(0xFFEEEEEE);

        Style.formTextColor               = Color(0xFFFFFFFF);
        Style.formTextErrorColor          = Color(0xFF00A9F4);
        Style.formBorderColor             = Color(0xFF777777);
        Style.formBorderFocusedColor      = Color(0xFFFFFFFF);
        Style.formBorderFocusedErrorColor = Color(0xFF00A9F4);
        Style.formBorderErrorColor        = Color(0xFF00A9F4);
        Style.formRadioActiveColor        = Color(0xFF00A9F4);

        SystemChrome.setSystemUIOverlayStyle(SystemUiOverlayStyle(
          statusBarColor: Color(0xFF2B2B2B),
          statusBarBrightness: Brightness.dark,
        ));
        SystemChrome.setPreferredOrientations([DeviceOrientation.portraitUp]);
        break;

      default:
        Style.appBarBackground                   = Color(0xFF00A9F4);
        Style.appBarBackgroundHome               = Color(0xFFFFFFFF);
        Style.appBarIconColor                    = Color(0xFF00A9F4);
        Style.appBarBorderBottomColor            = Color(0xFFEEEEEE);
        Style.drawerBackgroundColor              = Color(0xFFFFFFFF);
        Style.drawerNavigationColor              = Color(0xDD000000);
        Style.drawerNavigationBorderColor        = Color(0xFFEEEEEE);
        Style.scaffoldBackgroundColor            = Color(0xFFFFFFFF);
        Style.jokeName                           = Color(0xDD000000);
        Style.jokeText                           = Color(0xFF757575);
        Style.dividerColor                       = Color(0xFFDDDDDD);
        Style.settingsTitle                      = Color(0xDD000000);
        Style.bottomNavigationBarBackgroundColor = Color(0xFF00A9F4);
        Style.bottomNavigationBarColor           = Color(0xFFEEEEEE);

        Style.formTextColor               = Color(0xFF757575);
        Style.formTextErrorColor          = Color(0xFF00A9F4);
        Style.formBorderColor             = Color(0xFFDDDDDD);
        Style.formBorderFocusedColor      = Color(0xFF999999);
        Style.formBorderFocusedErrorColor = Color(0xFF00A9F4);
        Style.formBorderErrorColor        = Color(0xFF00A9F4);
        Style.formRadioActiveColor        = Color(0xFF00A9F4);

        SystemChrome.setSystemUIOverlayStyle(SystemUiOverlayStyle(
          statusBarColor: Color(0xFF00A9F4),
          statusBarBrightness: Brightness.light,

        ));
        SystemChrome.setPreferredOrientations([DeviceOrientation.portraitUp]);
        break;
    }
  }

  // ---------------------------------------------------------------------------

  static Future<String> getTheme() async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    String themeName = prefs.get('theme');
    if(themeName == null) {
      themeName = Constants.defaultTheme;
    }

    return themeName;
  }

  // ---------------------------------------------------------------------------

  static Future<void> setThemePrefs(String theme) async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    await prefs.setString('theme', theme);
  }

  // ---------------------------------------------------------------------------
}