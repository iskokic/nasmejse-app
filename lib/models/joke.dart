import 'dart:convert';
import 'package:nasmejse/models/constants.dart';
import 'package:nasmejse/models/user.dart';
import 'package:nasmejse/services/webservice.dart';
import 'package:shared_preferences/shared_preferences.dart';

class Joke {

  final String id;
  final String name;
  final String text;
  final String date;
  final String categoryId;
  final String categoryName;

  Joke(this.id, this.name, this.text, this.date, this.categoryId, this.categoryName);

  // ---------------------------------------------------------------------------

  Joke.fromJson(Map<String, dynamic> json):
        id           = json['id'],
        name         = json['name'],
        text         = json['text'],
        date         = json['date'],
        categoryId   = json['category_id'],
        categoryName = json['category_name'];

  // ---------------------------------------------------------------------------

  static Future<List<Joke>> getList(int pageNumber, String categoryId) async {

    String sort = await getSort();

    var params = {'token' : 'nasmejse', 'page_number': '$pageNumber', 'category_id': categoryId, 'sort' : sort};
    Webservice webservice = await Webservice.getData(Constants.urlJokeList, params);
    List collection = [];
    List<Joke> _jokes;
    if(webservice.status){
      collection = json.decode(webservice.data)['data'];
      _jokes = collection.map((json) => Joke.fromJson(json)).toList();
    }

    return _jokes;
  }

  // ---------------------------------------------------------------------------

  static Future<String> getSort() async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    final sortCheck = prefs.get('jokeSort');
    if(sortCheck == null) {
        return 'date';
    }
    return sortCheck;
  }

  // ---------------------------------------------------------------------------

  static Future<void> setSort(String sortSet) async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    await prefs.setString('jokeSort', sortSet);
  }

  // ---------------------------------------------------------------------------

  static Future<Webservice> rating(String joke, double rating) async {

    String user = await User.checkUser();
    //await Future.delayed(Duration(seconds: 1), () {});
    var params = {'token' : 'nasmejse', 'joke': '$joke', 'rating' : '$rating', 'user' : '$user'};
    Webservice webservice = await Webservice.getData(Constants.urlJokeRating, params);
    //bool status = webservice.status;

    return webservice;
  }

  // ---------------------------------------------------------------------------

  static Future<bool> send(String name, String email, String text) async {
    await Future.delayed(Duration(seconds: 2), () {});
    var params = {'token' : 'nasmejse', 'name': name, 'email' : email, 'text' : text};
    Webservice webservice = await Webservice.getData(Constants.urlJokeSend, params);
    bool status = webservice.status;

    return status;
  }

  // ---------------------------------------------------------------------------
}